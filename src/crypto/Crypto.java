package crypto;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
/**
 *
 * @author oshi
 */
public class Crypto {

    public static void main(String[] args) throws UnsupportedEncodingException {

        BigInteger[] keys = generateKey();
        BigInteger n = keys[0];
        BigInteger publicKey = keys[1];
        BigInteger privatekey = keys[2];
        String publicKeyString = n+" "+publicKey;
        String privateKeyString = n+" "+privatekey;
        String original = "Random House is the largest general-interest trade book publisher in the world.[1][2][3] As of 2013, it is part of Penguin" ;              //Random House, which is jointly owned by German media conglomerate Bertelsmann and global education and publishing company Pearson PLC.";
//        String original = "this is a test";
        BigInteger cipher = encrypt(original, publicKey, n);
        System.out.println("Cipher : "+cipher);
        System.out.println("");
        System.out.print("Plain text : ");
        System.out.println(decrypt(cipher, privatekey, n));
        System.out.println("");

    }

    public static BigInteger getModulo(BigInteger number, BigInteger modWorld) {

        if (number.compareTo(BigInteger.ZERO) == 0 || number.compareTo(BigInteger.ZERO) == 1) {
            BigInteger temp = number.divide(modWorld);
            BigInteger mod = number.subtract(modWorld.multiply(temp));
            return mod;
        } else {
            BigInteger temp = number.negate().divide(modWorld);

            if ((temp.multiply(modWorld)).compareTo(number.negate()) == 0) {
                return BigInteger.ZERO;
            } else {
                return modWorld.subtract(number.negate().subtract(temp.multiply(modWorld)));
            }
        }

    }

    public static BigInteger getModPow(BigInteger number, BigInteger modWorld, BigInteger exponent) {

        List<Integer> binaryList = getBinaryList(exponent);
        BigInteger mod = BigInteger.ONE;
        for (int i : binaryList) {
            mod = mod.multiply(number.pow(i));
        }
        mod = getModulo(mod, modWorld);
        return mod;

    }

    public static List<Integer> getBinaryList(BigInteger bigInt) {
        List<Integer> binaryList = new ArrayList<Integer>();
        String binaryString = bigInt.toString(2);

        int count = 0;
        for (int i = binaryString.length() - 1; i >= 0; i--) {

            if (binaryString.charAt(i) == '1') {
                binaryList.add((int) Math.pow(2, count));
            }
            count++;

        }

        return binaryList;
    }

    public static BigInteger getGcd(BigInteger m, BigInteger n) {
        if (n.compareTo(BigInteger.ZERO) == 0) {
            return m;
        } else {
            return getGcd(n, getModulo(m, n));
        }

    }

    public static boolean isCoPrime(BigInteger m, BigInteger n) {
        return getGcd(m, n).compareTo(BigInteger.ONE) == 0;
    }

    public static BigInteger[] extEuclidean(BigInteger a, BigInteger b) {
        BigInteger quotient = BigInteger.ONE;
        BigInteger[] ans = new BigInteger[3];
        if (!(b.equals(BigInteger.ZERO))) {
            quotient = a.divide(b);
            ans = extEuclidean(b, getModulo(a, b));
            BigInteger temp = ans[1].subtract((ans[2]).multiply(quotient));
            ans[1] = ans[2];
            ans[2] = temp;
        } else if (b.equals(BigInteger.ZERO)) {
            ans[0] = a;
            ans[1] = BigInteger.ONE;
            ans[2] = BigInteger.ZERO;
        }
        return ans;
    }

    public static BigInteger modInverse(BigInteger a, BigInteger b) {
        //a = mod world, b = value
        return getModulo(extEuclidean(a, b)[2], a);
    }

    public static BigInteger encrypt(String message, BigInteger e, BigInteger n) throws UnsupportedEncodingException {
        BigInteger plainText = new BigInteger(message.getBytes(StandardCharsets.UTF_8));
        return plainText.modPow(e, n);
//return plainText;
    }

    public static String decrypt(BigInteger c, BigInteger d, BigInteger n) throws UnsupportedEncodingException {

        BigInteger originalText = c.modPow(d, n);
        return new String(originalText.toByteArray(), StandardCharsets.UTF_8);
    }

    public static BigInteger[] generateKey() {
        BigInteger[] key = new BigInteger[3];
        Random rand = new Random();
        BigInteger p = BigInteger.probablePrime(500, rand);
        BigInteger q = BigInteger.probablePrime(500, rand);
        BigInteger n = p.multiply(q);
        BigInteger e = BigInteger.probablePrime(500, rand);
        BigInteger phi = (p.subtract(BigInteger.ONE)).multiply(q.subtract(BigInteger.ONE));
        while (!isCoPrime(e, phi)) {
            e = BigInteger.probablePrime(500, rand);
        }
        BigInteger d = modInverse(phi, e);
        key[0] = n; // mod
        key[1] = e; //public key
        key[2] = d; //private key

        return key;
    }

}
